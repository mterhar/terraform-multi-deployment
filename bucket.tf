resource "aws_s3_bucket" "b" {
  bucket = var.BUCKET_NAME
  acl    = "private"

  tags = {
    Name        = "Max Bucket N"
    Application = "Mighty Bucket"
    Mood        = "Happy"
    Tier        = "III"
  }
}
